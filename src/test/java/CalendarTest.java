import org.junit.Assert;
import org.junit.Test;

public class CalendarTest {


/*    здесь задаём ЛЮБЫЕ три значения, которые необходимо протестировать на валидность,
    исходя из условий, нам было необходимо протестировать на валидность значения даты: 31, 3, 1988. */

    private int testDateDay = 31;
    private int testDateMonth = 3;
    private int testDateYear = 1988;


    @Test
    //сам тест проверяющий заданые пользователем значения на валидность:
    public void calendarTest(){
        Assert.assertTrue((char) 27 + "[31mВведённая дата некорректна! " + (char)27 + "[0m", isCalendarValid(new Calendar(testDateDay, testDateMonth, testDateYear)));
    }

    //метод для проверки валидности:
    private boolean isCalendarValid(Calendar calendar) {
        //условия проверки на нулевое значение:
        if (calendar == null)
            return false;
        //значение года меньше нуля:
        if (calendar.getYear() < 0)
            return false;
        //значение месяца выходит за рамки 1 до 12:
        if (calendar.getMonth() < 1 || calendar.getMonth() > 12)
            return false;
        //значение дня меньше 1, ИЛИ значение дня выходит за рамки условий, количества дней в месяце
        if (calendar.getDay() < 1 || calendar.getDay() >
                getDaysCountForMonth(calendar.getMonth(), calendar.getYear())) {
            return false;
        }

        return true;
    }

    //метод возвращающий количество дней в соответствующем месяце:
    private int getDaysCountForMonth(int month, int year) {
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                return (year % 4 == 0 && year % 100 != 0 && year % 400
                        == 0) ? 29 : 28;
        }
        return 31;
    }
}